﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using NurseryS.Models;

namespace NurseryS.Context
{
    public partial class DataContext : DbContext
    {
        public DataContext() : base("name=NorthOaksDB")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Tag> Tags { get; set; }
    }
}
