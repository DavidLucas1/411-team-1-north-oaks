﻿using NurseryS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NurseryS.Infrastructure
{
    public static class Extensions
    {
        public static RestDataContext rdc = new RestDataContext();

        public static List<IServerDataModel> changedObs = new List<IServerDataModel>();
        public static List<IServerDataModel> deletedObs = new List<IServerDataModel>();
        public static T Find<T>(this IEnumerable<T> data, object keyValue) where T : IServerDataModel
        {
            return (T)rdc.FindByID(typeof(T), keyValue);
            //return data.First(dat => dat.primaryKey.Equals(keyValue));
        }    

        public static IEnumerable<T> Add<T>(this IEnumerable<T> list, T elem) where T : IServerDataModel
        {
            changedObs.Add(elem); 
            return list.Concat(new T[]{elem});
        }

        public static void Remove<T>(this IEnumerable<T> list, T elem) where T : IServerDataModel
        {
            deletedObs.Add(elem);
            list = list.Except(new T[] { elem });
        }

        public static void Update<T>(this IEnumerable<T> list, T elem) where T : IServerDataModel
        {
            rdc.Update(elem);
            if(changedObs.Contains(elem))
            {
                changedObs.Remove(elem);
            }
        }
    }
}