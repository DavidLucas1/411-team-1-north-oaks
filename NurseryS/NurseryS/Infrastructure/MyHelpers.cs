﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NurseryS.Infrastructure
{
    public static class MyHelpers
    {
        public static MvcHtmlString Image(this HtmlHelper helper,
            string src, string altText)
        {
            var builder = new TagBuilder("img");
            builder.MergeAttribute("src", src);
            builder.MergeAttribute("alt", altText);
            return MvcHtmlString.Create(builder.ToString(TagRenderMode.SelfClosing));
        }
       
        public static MvcHtmlString MyValidationSummary(this HtmlHelper helper) 
        {
            string retVal = "";
            if (helper.ViewData.ModelState.IsValid)
                return new MvcHtmlString("");

            retVal += "<div class='notification-warnings'>"; 
            foreach (var key in helper.ViewData.ModelState.Keys)
            {
                foreach (var err in helper.ViewData.ModelState[key].Errors)
                    retVal += "<p style=\"color: red\">" + helper.Encode(err.ErrorMessage) + "</p>";
            }
            retVal += "</div>";
            return new MvcHtmlString(retVal.ToString());
        }
        
    }
}