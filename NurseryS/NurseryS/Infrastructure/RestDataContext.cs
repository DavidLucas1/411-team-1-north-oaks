﻿using NurseryS.Models;
using RestSharp;
using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace NurseryS.Infrastructure
{
    
    public class RestDataContext 
    {
        /// <summary>
        /// This should act exactly like a usual DataContext in *almost* every single way.
        /// One key difference, is that SaveChanges will not update a CHANGED row, but only ADDED and REMOVED rows in the current transaction
        /// To update a CHANGED row in the database, you must directly call DataContext.Table.Update(object changedOb)
        /// Example usage: DataContext.Accounts.Update(account)
        /// </summary>
        private ServerDataRestClient<Comment> commentClient = new ServerDataRestClient<Comment>();
        private ServerDataRestClient<Organization> orgClient = new ServerDataRestClient<Organization>();
        private ServerDataRestClient<Post> postClient = new ServerDataRestClient<Post>();
        private ServerDataRestClient<Role> roleClient = new ServerDataRestClient<Role>();
        private ServerDataRestClient<Account> accountClient = new ServerDataRestClient<Account>();
        private ServerDataRestClient<Tag> tagClient = new ServerDataRestClient<Tag>();
        private ServerDataRestClient<apiKey> apiClient = new ServerDataRestClient<apiKey>();

        private IEnumerable<Comment> _Comments;
        private IEnumerable<Organization> _Organizations;
        private IEnumerable<Post> _Posts;
        private IEnumerable<Role> _Roles;
        private IEnumerable<Account> _Accounts;
        private IEnumerable<Tag> _Tags;
        private IEnumerable<apiKey> _apiKeys;
        

        public IEnumerable<Comment> Comments 
        {
            get
            {
                if(_Comments != null)
                {
                    return _Comments;
                }
                else
                {
                    _Comments = commentClient.GetAll();
                    return _Comments;
                }
            }
            set
            {
                _Comments = value;
            }
        }
        public IEnumerable<Organization> Organizations 
        {
            get
            {
                if (_Organizations != null)
                {
                    return _Organizations;
                }
                else
                {
                    _Organizations = orgClient.GetAll();
                    return _Organizations;
                }
            }
            set
            {
                _Organizations = value;
            }
        }
        public IEnumerable<Post> Posts
        {
            get
            {
                if (_Posts != null)
                {
                    return _Posts;
                }
                else
                {
                    _Posts = postClient.GetAll();
                    return _Posts;
                }
            }
            set
            {
                _Posts = value;
            }
        }
        public IEnumerable<Role> Roles 
        {
            get
            {
                if (_Roles != null)
                {
                    return _Roles;
                }
                else
                {
                    _Roles = roleClient.GetAll();
                    return _Roles;
                }
            }
            set
            {
                _Roles = value;
            }
        }
        public IEnumerable<Account> Accounts 
        {
            get
            {
                if (_Accounts != null)
                {
                    return _Accounts;
                }
                else
                {
                    _Accounts = accountClient.GetAll();
                    return _Accounts;
                }
            }
            set
            {
                _Accounts = value;
            }
        }
        public IEnumerable<Tag> Tags 
        {
            get
            {
                if (_Tags != null)
                {
                    return _Tags;
                }
                else
                {
                    _Tags = tagClient.GetAll();
                    return _Tags;
                }
            }
            set
            {
                _Tags = value;
            }
        }
        public IEnumerable<apiKey> apiKeys 
        {
            get
            {
                if (_apiKeys != null)
                {
                    return _apiKeys;
                }
                else
                {
                    _apiKeys = apiClient.GetAll();
                    return _apiKeys;
                }
            }
            set
            {
                _apiKeys = value;
            }
        }

        public RestDataContext()
        {
            
        }

        public IServerDataModel FindByID(Type typeOb, object id)
        {
            if(typeOb == typeof(Comment))
                return commentClient.GetByKey(id);
            if (typeOb == typeof(Organization))
                return orgClient.GetByKey(id);
            if (typeOb == typeof(Post))
                return postClient.GetByKey(id);
            if (typeOb == typeof(Role))
                return roleClient.GetByKey(id);
            if (typeOb == typeof(Account))
                return accountClient.GetByKey(id);
            if (typeOb == typeof(Tag))
                return tagClient.GetByKey(id);
            if (typeOb == typeof(apiKey))
                return apiClient.GetByKey(id);

            return null;
        }

        public EntityEntry Entry(IServerDataModel item)
        {
            return new EntityEntry(item);
        }

        public void Update(object changedOb)
        {
            Type obType = changedOb.GetType();
            if (obType == typeof(Comment))
                commentClient.Update((Comment)changedOb);
            else if (obType == typeof(Organization))
                orgClient.Update((Organization)changedOb);
            else if (obType == typeof(Post))
                postClient.Update((Post)changedOb);
            else if (obType == typeof(Role))
                roleClient.Update((Role)changedOb);
            else if (obType == typeof(Account))
                accountClient.Update((Account)changedOb);
            else if (obType == typeof(Tag))
                tagClient.Update((Tag)changedOb);
            else if (obType == typeof(apiKey))
                apiClient.Update((apiKey)changedOb);
            else
                throw new Exception("Error in Update(IServerDataModel object), object is not a recognized type.  You may need to update your RestDataContext Definitions");
            
           
        }

        public void SaveChanges()
        {
            foreach(IServerDataModel ob in Extensions.changedObs)
            {
                if(ob is Comment)
                {
                    commentClient.Update((Comment)ob);
                }
                else if(ob is Organization)
                {
                    orgClient.Update((Organization)ob);
                }
                else if(ob is Post)
                {
                    postClient.Update((Post)ob);
                }
                else if(ob is Role)
                {
                    roleClient.Update((Role)ob);
                }
                else if(ob is Account)
                {
                    accountClient.Update((Account)ob);
                }
                else if(ob is Tag)
                {
                    tagClient.Update((Tag)ob);
                }
                else if(ob is apiKey)
                {
                    apiClient.Update((apiKey)ob);
                }
            }

            foreach (IServerDataModel ob in Extensions.deletedObs)
            {
                if (ob is Comment)
                {
                    commentClient.Delete(ob.primaryKey);
                }
                else if (ob is Organization)
                {
                    orgClient.Delete(ob.primaryKey);
                }
                else if (ob is Post)
                {
                    postClient.Delete(ob.primaryKey);
                }
                else if (ob is Role)
                {
                    roleClient.Delete(ob.primaryKey);
                }
                else if (ob is Account)
                {
                    accountClient.Delete(ob.primaryKey);
                }
                else if (ob is Tag)
                {
                    tagClient.Delete(ob.primaryKey);
                }
                else if (ob is apiKey)
                {
                    apiClient.Delete(ob.primaryKey);
                }
            }

            Extensions.changedObs = new List<IServerDataModel>();
            Extensions.deletedObs = new List<IServerDataModel>();
        }

        public void Dispose()
        {
            this.Comments = null;
            this.Organizations = null;
            this.Posts = null;
            this.Roles = null;
            this.Accounts = null;
            this.Tags = null;
            this.apiKeys = null;
        }
    }

    public class EntityEntry
    {
        public IServerDataModel Entity { get; set; }
        private EntityState _State { get; set; }
        public EntityState State 
        {
            get
            {
                return _State;
            }
            set
            {
                _State = value;
                if(value == EntityState.Modified || value == EntityState.Added || value == EntityState.Detached)
                {
                    if(!Extensions.changedObs.Contains(this.Entity))
                    {
                        Extensions.changedObs.Add(this.Entity);
                    }
                }
                if(value == EntityState.Deleted)
                {
                    if(!Extensions.deletedObs.Contains(this.Entity))
                    {
                        Extensions.deletedObs.Add(this.Entity);
                    }
                }
            }
        }
        public IServerDataModel EntityBeforeChanges {get; set;}
        

        public EntityEntry(IServerDataModel Entity)
        {
            this.Entity = Entity;
            State = EntityState.Unchanged;
            Type tp = Entity.GetType();
            if (tp == typeof(Comment))
            {
                ServerDataRestClient<Comment> client = new ServerDataRestClient<Comment>();
                EntityBeforeChanges = client.GetByKey(Entity.primaryKey);
            }
            else if (tp == typeof(Organization))
            {
                ServerDataRestClient<Organization> client = new ServerDataRestClient<Organization>();
                EntityBeforeChanges = client.GetByKey(Entity.primaryKey);
            }
            else if (tp == typeof(Post))
            {
                ServerDataRestClient<Post> client = new ServerDataRestClient<Post>();
                EntityBeforeChanges = client.GetByKey(Entity.primaryKey);
            }
            else if (tp == typeof(Role))
            {
                ServerDataRestClient<Role> client = new ServerDataRestClient<Role>();
                EntityBeforeChanges = client.GetByKey(Entity.primaryKey);
            }
            else if (tp == typeof(Account))
            {
                ServerDataRestClient<Account> client = new ServerDataRestClient<Account>();
                EntityBeforeChanges = client.GetByKey(Entity.primaryKey);
            }
            else if (tp == typeof(Tag))
            {
                ServerDataRestClient<Tag> client = new ServerDataRestClient<Tag>();
                EntityBeforeChanges = client.GetByKey(Entity.primaryKey);
            }
            else if (tp == typeof(apiKey))
            {
                ServerDataRestClient<apiKey> client = new ServerDataRestClient<apiKey>();
                EntityBeforeChanges = client.GetByKey(Entity.primaryKey);
            }
            else
            {
                throw new Exception(tp.Name + " is not a supported type");
            }
            
            
        }
    }

    public abstract class IServerDataModel
    {
        /// <summary>
        /// All child classes MUST override primaryKey.  This is only non-abstract to allow for lists of ServerDataRestClients
        /// </summary>
        abstract public object primaryKey { get; }

    }

    public interface IServerDataRestClient<T> where T : IServerDataModel
    {
        
        void Add(T serverData);
        void Delete(object id);
        IEnumerable<T> GetAll();
        T GetByKey(object keyValue);
        void Update(T newObj);
    }

    public class ServerDataRestClient<T> : IServerDataRestClient<T> where T : IServerDataModel, new()
    {
        readonly RestClient _client;
        readonly string _url = "http://localhost:49292/";
        readonly string header = "tlscwUlGSOlgPosOfbcZiww";
        readonly string modelType;
        
        public ServerDataRestClient()
        {
            _client = new RestClient { BaseUrl = new Uri(_url) };
            Type type = typeof(T);
            this.modelType = type.Name;

        }

        public IEnumerable<T> GetAll()
        {

            var request = new RestRequest("api/" + modelType, Method.GET) { RequestFormat = DataFormat.Json };
            request.AddHeader("apiKey", header);

            var response = _client.Execute<List<T>>(request);


            if (response.Data == null)
                throw new Exception(response.ErrorMessage);

            JsonDeserializer deserial = new JsonDeserializer();
            return deserial.Deserialize<List<T>>(response);
            //return new RestDbSet<Comment>(response.Data);
        
        }

        public T GetByKey(object keyValue) 
        {
            var request = new RestRequest("api/" + modelType + "/{id}", Method.GET) { RequestFormat = DataFormat.Json };
            request.AddHeader("apiKey", header);
            request.AddParameter("id", keyValue, ParameterType.UrlSegment);

            var response = _client.Execute<T>(request);

            if (response.Data == null)
                throw new Exception(response.ErrorMessage);

            JsonDeserializer deserial = new JsonDeserializer();
            return deserial.Deserialize<T>(response);
            //return response.Data;
        }

        public void Add(T newObj)
        {
            var request = new RestRequest("api/" + modelType, Method.POST) { RequestFormat = DataFormat.Json };
            request.AddHeader("apiKey", header);
            request.AddBody(newObj);

            var response = _client.Execute<T>(request);

            if (response.StatusCode != HttpStatusCode.Created)
                throw new Exception(response.ErrorMessage);
        }

        public void Update(T newObj)
        {
            var request = new RestRequest("api/" + modelType + "/{id}", Method.PUT) { RequestFormat = DataFormat.Json };
            request.AddHeader("apiKey", header);
            request.AddParameter("id", newObj.primaryKey, ParameterType.UrlSegment);
            request.AddBody(newObj);

            var response = _client.Execute<T>(request);

            if (response.StatusCode == HttpStatusCode.NotFound)
                throw new Exception(response.ErrorMessage);
        }

        public void Delete(object id)
        {
            var request = new RestRequest("api/" + modelType + "/{id}", Method.DELETE);
            request.AddHeader("apiKey", header);
            request.AddParameter("id", id, ParameterType.UrlSegment);

            var response = _client.Execute<T>(request);

            if (response.StatusCode == HttpStatusCode.NotFound)
                throw new Exception(response.ErrorMessage);
        }
    }
        
}