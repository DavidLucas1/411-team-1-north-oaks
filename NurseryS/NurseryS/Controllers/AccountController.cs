﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NurseryS.Models;
using NurseryS.Context;
using NurseryS.ViewModels;
using System.Web.Security;
using System.Text;

namespace NurseryS.Controllers
{
    public class AccountController : Controller
    {
        private DataContext db = new DataContext();

        //
        // GET: /Account/

        [Authorize(Roles="Admin")]
        public ActionResult Index()
        {
            return View(db.Accounts.ToList());
        }

        //
        // GET: /Account/Details/5

        [Authorize]
        public ActionResult Details(string username = null)
        {
            if(username == null && User.Identity.IsAuthenticated)
            {
                username = User.Identity.Name;
            }
            if(username == null && !User.Identity.IsAuthenticated)
            {
                return HttpNotFound();
            }
            Account account = db.Accounts.FirstOrDefault(ac => ac.UserName.Equals(username));
 
            if (account == null)
            {
                return HttpNotFound();
            }

            List<Post> posts = account.Posts.OrderByDescending(ps => ps.ReleaseDate).ToList();

            AccountDetailsViewModel advm = new AccountDetailsViewModel();
            advm.account = account;
            advm.posts = posts;

            return View(advm);
        }

        //
        // GET: /Account/Create

        //[RequireHttps]
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Account/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        //[RequireHttps]
        public ActionResult Create(AccountViewModel avm, string returnUrl)
        {
            if (avm.account.UserName != null && avm.account.UserName.Length > 0)
            {
                Account exists = db.Accounts.SingleOrDefault(ac => ac.UserName.Equals(avm.account.UserName));
                if (exists != null)
                {
                    // Account already exists.  fail back
                    ModelState.AddModelError("invalidUsername", "That username is already taken.  Please choose a different one");
                }
            }

            if (ModelState.IsValid)
            {
                avm.account.Register(avm.account.UserName, avm.passTemp, avm.account.FirstName, avm.account.LastName, avm.account.Email);

                
                if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                    && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                {
                    return Redirect(returnUrl);
                }
                else
                {
                 
                    return RedirectToAction("Index", "Home");
                }
            }

            
            
            return View(avm);
        }

        public ActionResult Login()
        {
            //LoginViewModel lm = new LoginViewModel();
            return View();
        }

        //
        // POST: /Account/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel lm, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                bool success = Account.Login(lm.UserName, lm.Password);

                if (success)
                {
                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                        && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ViewBag.FailMessage = "That username/password combination is invalid";
                    return View(lm);
                }

            }

            return View(lm);
        }

        public ActionResult Logoff()
        {
            Account.Logout();

            //LoginViewModel lm = new LoginViewModel();
            Session.Clear();
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/Edit/5

        [Authorize]
        public ActionResult Edit(string id = null)
        {
            Account account = db.Accounts.Find(id);
            if (account == null)
            {
                return HttpNotFound();
            }
            return View(account);
        }

        //
        // POST: /Account/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit(Account account)
        {

            if (ModelState.IsValid)
            {
                db.Entry(account).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Details");
            }
            return View(account);
        }

        //
        // GET: /Account/Delete/5

        [Authorize(Roles="Admin")]
        public ActionResult Delete(string id = null)
        {
            Account account = db.Accounts.Find(id);
            if (account == null)
            {
                return HttpNotFound();
            }
            return View(account);
        }

        //
        // POST: /Account/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles="Admin")]
        public ActionResult DeleteConfirmed(string id)
        {
            Account account = db.Accounts.Find(id);
            db.Accounts.Remove(account);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}