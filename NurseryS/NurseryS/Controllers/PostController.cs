﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NurseryS.Models;
using NurseryS.Context;
using NurseryS.Infrastructure;

namespace NurseryS.Controllers
{
    public class PostController : Controller
    {
        private DataContext db = new DataContext();

        //
        // GET: /Post/

        public ActionResult Index()
        {
            return View(db.Posts.Where(m => m.Account.UserName.Equals(User.Identity.Name)).ToList());
        }

        //
        // GET: /Post/Details/5

        public ActionResult Details(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // GET: /Post/Create

        [Authorize(Roles="Admin")]
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Post/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles="Admin")]
        [ValidateInput(false)] 
        public ActionResult Create(Post post)
        {
            if (ModelState.IsValid)
            {
                post.Account = db.Accounts.SingleOrDefault(a => a.UserName.Equals(HttpContext.User.Identity.Name));
                post.CreateDate = DateTime.Now;
                db.Posts.Add(post);
                db.SaveChanges();
                return RedirectToAction("Index", "Home", null);
            }

            
            return View(post);
        }

        //
        // GET: /Post/Edit/5

        [Authorize(Roles="Admin")]
        public ActionResult Edit(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles="Admin")]
        public ActionResult Edit(Post post)
        {
            if (ModelState.IsValid)
            {
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Details", new { id = post.ID });
            }
            return View(post);
        }

        //
        // GET: /Post/Delete/5

        [Authorize(Roles="Admin")]
        public ActionResult Delete(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles="Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}