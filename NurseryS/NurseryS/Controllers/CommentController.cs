﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NurseryS.Models;
using NurseryS.Context;
using NurseryS.ViewModels;

namespace NurseryS.Controllers
{
    public class CommentController : Controller
    {
        private DataContext db = new DataContext();

        [HttpPost]
        [Authorize]
        public ActionResult Create(int? postID, CommentViewModel cvm)
        {

            if (cvm.comment == null)
            {
                cvm.comment = new Comment();
                cvm.post = db.Posts.Find(postID);
                cvm.comment.Post = cvm.post;
                cvm.comment.Account = db.Accounts.First(ac => ac.UserName == User.Identity.Name);
                return View(cvm);
            }
            else
            {
                cvm.comment.Post = db.Posts.Find(postID);
                cvm.comment.Account = db.Accounts.First(ac => ac.UserName == User.Identity.Name);
                cvm.comment.CreateDate = DateTime.Now;
                db.Comments.Add(cvm.comment);
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }

        }



        [Authorize]
        public ActionResult Delete(int id = 0)
        {
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(comment);
        }

        //
        // POST: /Comment/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DeleteConfirmed(int id)
        {
            Comment comment = db.Comments.Find(id);
            db.Comments.Remove(comment);
            db.SaveChanges();
            return RedirectToAction("Index", "Home");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}