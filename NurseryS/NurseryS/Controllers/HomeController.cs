﻿using NurseryS.Context;
using NurseryS.Infrastructure;
using NurseryS.Models;
using NurseryS.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NurseryS.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        RestDataContext db = new RestDataContext();
        

        public ActionResult Index()
        {
            HomeViewModel hvm = new HomeViewModel();
            List<Post> posts = db.Posts.Where(post => DateTime.Compare(post.ReleaseDate, DateTime.Now) < 0).ToList();
            posts.Reverse();
            hvm.posts = posts;
            return View(hvm); 
        }


        


    }
}
