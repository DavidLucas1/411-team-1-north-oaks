﻿using NurseryS.ViewModels;
using System;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace NurseryS.Controllers
{
    [HandleError]
    public class ErrorController : Controller
    {
        public ActionResult Error()
        {
            ErrorViewModel evm = new ErrorViewModel();
            evm.Message = "An Error Has Occured";
            evm.Description = "An unexpected error occured on our website. The website administrator has been notified.";
            return PartialView(evm);
        }
        public ActionResult BadRequest()
        {
            ErrorViewModel evm = new ErrorViewModel();
            evm.Message = "Bad Request";
            evm.Description = "The request cannot be fulfilled due to bad syntax.";
            return PartialView("Error", evm);
        }
        public ActionResult NotFound()
        {
            ErrorViewModel evm = new ErrorViewModel();
            evm.Message = "We are sorry, the page you requested cannot be found.";
            evm.Description = "The URL may be misspelled or the page you're looking for is no longer available.";
            return PartialView("Error", evm);
        }

        public ActionResult Forbidden()
        {
            ErrorViewModel evm = new ErrorViewModel();
            evm.Message = "403 Forbidden";
            evm.Description = "Forbidden: You don't have permission to access [directory] on this server.";
            return PartialView("Error", evm);
        }
        public ActionResult URLTooLong()
        {
            ErrorViewModel evm = new ErrorViewModel();
            evm.Message = "URL Too Long";
            evm.Description = "The requested URL is too large to process. That’s all we know.";
            return PartialView("Error", evm);
        }
        public ActionResult ServiceUnavailable()
        {
            ErrorViewModel evm = new ErrorViewModel();
            evm.Message = "Service Unavailable";
            evm.Description = "Our apologies for the temporary inconvenience. This is due to overloading or maintenance of the server.";
            return PartialView("Error", evm);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}
