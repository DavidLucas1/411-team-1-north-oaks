namespace NurseryS.Models
{
    using NurseryS.Infrastructure;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    
    public partial class Post : IServerDataModel
    {
        public Post()
        {
            this.Tags = new HashSet<Tag>();
        }
    
        public int ID { get; set; }
        public System.DateTime CreateDate { get; set; }
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public System.DateTime ReleaseDate { get; set; }
        [Required]
        [MinLength(1)]
        public string Title { get; set; }
        [Required]
        [MinLength(1)]
        [AllowHtml]
        public string Text { get; set; }
    
        public virtual Account Account { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }

        public override object primaryKey
        {
            get { return ID; }
        }
    }
}
