namespace NurseryS.Models
{
    using NurseryS.Infrastructure;
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;

    public partial class Comment : IServerDataModel
    {
        public int ID { get; set; }
        [AllowHtml]
        public string Text { get; set; }
        public System.DateTime CreateDate { get; set; }
    
        public virtual Account Account { get; set; }
        public virtual Post Post { get; set; }

        public override object primaryKey
        {
            get { return ID; }
        }
    }
}
