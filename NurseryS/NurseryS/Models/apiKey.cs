﻿using NurseryS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NurseryS.Models
{
    public class apiKey : IServerDataModel
    {
        public int Id { get; set; }
        public string key { get; set; }

        public override object primaryKey
        {
            get { return Id; }
        }
    }
}