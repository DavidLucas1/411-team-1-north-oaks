namespace NurseryS.Models
{
    using NurseryS.Infrastructure;
    using System;
    using System.Collections.Generic;
    
    public partial class Tag : IServerDataModel
    {
        public Tag()
        {
            this.Posts = new HashSet<Post>();
        }

        public long ID { get; set; }
        public string Text { get; set; }
    
        public virtual ICollection<Post> Posts { get; set; }

        public override object primaryKey
        {
            get { return ID; }
        }
    }
}
