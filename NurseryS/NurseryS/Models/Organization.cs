namespace NurseryS.Models
{
    using NurseryS.Infrastructure;
    using System;
    using System.Collections.Generic;
    
    public partial class Organization : IServerDataModel
    {
        public Organization()
        {
            this.Accounts = new HashSet<Account>();
        }
    
        public int ID { get; set; }
        public string Name { get; set; }
        public string Website { get; set; }
        public string Description { get; set; }
    
        public virtual ICollection<Account> Accounts { get; set; }

        public override object primaryKey
        {
            get { return ID; }
        }
    }
}
