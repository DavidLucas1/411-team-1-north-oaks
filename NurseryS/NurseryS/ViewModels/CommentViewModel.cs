﻿using NurseryS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NurseryS.ViewModels
{
    public class CommentViewModel
    {
        public Comment comment { get; set; }
        public Post post { get; set; }
        public int postID { get; set; }
    }
}