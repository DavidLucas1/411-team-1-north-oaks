﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NurseryS.Models;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Web.Security;



namespace NurseryS.ViewModels
{
    public class AccountViewModel 
    {
        public Account account { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [DataType(DataType.Password)]
        [MinLength(6)]
        [Display(Name="Password")]
        public string passTemp { get; set; }

        [Required(ErrorMessage = "ConfirmPassword is required")]
        [DataType(DataType.Password)]
        [Compare("passTemp")]
        [Display(Name="ConfirmPassword")]
        public string passReTemp { get; set; }
    }
}