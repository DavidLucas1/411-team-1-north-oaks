﻿using NurseryS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NurseryS.ViewModels
{
    public class AccountDetailsViewModel
    {
        public Account account { get; set; }
        public List<Post> posts { get; set; }
    }
}