
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 04/27/2015 14:01:49
-- Generated from EDMX file: C:\Users\Kyle\Desktop\NOAPI\NorthOaks411\NorthOaks411\Migrations\NorthOakModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [NorthOaksDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_AccountComment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Comments] DROP CONSTRAINT [FK_AccountComment];
GO
IF OBJECT_ID(N'[dbo].[FK_AccountOrganization_Account]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AccountOrganization] DROP CONSTRAINT [FK_AccountOrganization_Account];
GO
IF OBJECT_ID(N'[dbo].[FK_AccountOrganization_Organization]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AccountOrganization] DROP CONSTRAINT [FK_AccountOrganization_Organization];
GO
IF OBJECT_ID(N'[dbo].[FK_AccountPost]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Posts] DROP CONSTRAINT [FK_AccountPost];
GO
IF OBJECT_ID(N'[dbo].[FK_AccountRole]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Accounts] DROP CONSTRAINT [FK_AccountRole];
GO
IF OBJECT_ID(N'[dbo].[FK_CommentPost]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Comments] DROP CONSTRAINT [FK_CommentPost];
GO
IF OBJECT_ID(N'[dbo].[FK_PostTag_Post]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PostTag] DROP CONSTRAINT [FK_PostTag_Post];
GO
IF OBJECT_ID(N'[dbo].[FK_PostTag_Tag]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PostTag] DROP CONSTRAINT [FK_PostTag_Tag];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[AccountOrganization]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AccountOrganization];
GO
IF OBJECT_ID(N'[dbo].[Accounts]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Accounts];
GO
IF OBJECT_ID(N'[dbo].[Comments]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Comments];
GO
IF OBJECT_ID(N'[dbo].[Organizations]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Organizations];
GO
IF OBJECT_ID(N'[dbo].[Posts]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Posts];
GO
IF OBJECT_ID(N'[dbo].[PostTag]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PostTag];
GO
IF OBJECT_ID(N'[dbo].[Roles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Roles];
GO
IF OBJECT_ID(N'[dbo].[Tags]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Tags];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Accounts'
CREATE TABLE [dbo].[Accounts] (
    [UserName] nvarchar(50)  NOT NULL,
    [Password] varbinary(max)  NOT NULL,
    [Email] nvarchar(50)  NOT NULL,
    [Website] nvarchar(max)  NULL,
    [FirstName] nvarchar(50)  NOT NULL,
    [LastName] nvarchar(50)  NOT NULL,
    [FullName] nvarchar(50)  NULL,
    [Salt] varbinary(max)  NOT NULL,
    [Role_ID] int  NOT NULL
);
GO

-- Creating table 'Comments'
CREATE TABLE [dbo].[Comments] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Text] nvarchar(max)  NOT NULL,
    [CreateDate] datetime  NOT NULL,
    [Account_UserName] nvarchar(50)  NOT NULL,
    [Post_ID] int  NOT NULL
);
GO

-- Creating table 'Organizations'
CREATE TABLE [dbo].[Organizations] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Website] nvarchar(max)  NULL,
    [Description] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Posts'
CREATE TABLE [dbo].[Posts] (
    [ID] int  NOT NULL,
    [CreateDate] datetime  NOT NULL,
    [ReleaseDate] datetime  NOT NULL,
    [Text] nvarchar(max)  NOT NULL,
    [Account_UserName] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'Roles'
CREATE TABLE [dbo].[Roles] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [RoleName] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Tags'
CREATE TABLE [dbo].[Tags] (
    [ID] bigint IDENTITY(1,1) NOT NULL,
    [Text] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'apiKeys'
CREATE TABLE [dbo].[apiKeys] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [key] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'AccountOrganization'
CREATE TABLE [dbo].[AccountOrganization] (
    [Accounts_UserName] nvarchar(50)  NOT NULL,
    [Organizations_ID] int  NOT NULL
);
GO

-- Creating table 'PostTag'
CREATE TABLE [dbo].[PostTag] (
    [Posts_ID] int  NOT NULL,
    [Tags_ID] bigint  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [UserName] in table 'Accounts'
ALTER TABLE [dbo].[Accounts]
ADD CONSTRAINT [PK_Accounts]
    PRIMARY KEY CLUSTERED ([UserName] ASC);
GO

-- Creating primary key on [ID] in table 'Comments'
ALTER TABLE [dbo].[Comments]
ADD CONSTRAINT [PK_Comments]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Organizations'
ALTER TABLE [dbo].[Organizations]
ADD CONSTRAINT [PK_Organizations]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Posts'
ALTER TABLE [dbo].[Posts]
ADD CONSTRAINT [PK_Posts]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Roles'
ALTER TABLE [dbo].[Roles]
ADD CONSTRAINT [PK_Roles]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Tags'
ALTER TABLE [dbo].[Tags]
ADD CONSTRAINT [PK_Tags]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [Id] in table 'apiKeys'
ALTER TABLE [dbo].[apiKeys]
ADD CONSTRAINT [PK_apiKeys]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Accounts_UserName], [Organizations_ID] in table 'AccountOrganization'
ALTER TABLE [dbo].[AccountOrganization]
ADD CONSTRAINT [PK_AccountOrganization]
    PRIMARY KEY CLUSTERED ([Accounts_UserName], [Organizations_ID] ASC);
GO

-- Creating primary key on [Posts_ID], [Tags_ID] in table 'PostTag'
ALTER TABLE [dbo].[PostTag]
ADD CONSTRAINT [PK_PostTag]
    PRIMARY KEY CLUSTERED ([Posts_ID], [Tags_ID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Role_ID] in table 'Accounts'
ALTER TABLE [dbo].[Accounts]
ADD CONSTRAINT [FK_AccountRole]
    FOREIGN KEY ([Role_ID])
    REFERENCES [dbo].[Roles]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AccountRole'
CREATE INDEX [IX_FK_AccountRole]
ON [dbo].[Accounts]
    ([Role_ID]);
GO

-- Creating foreign key on [Account_UserName] in table 'Posts'
ALTER TABLE [dbo].[Posts]
ADD CONSTRAINT [FK_AccountPost]
    FOREIGN KEY ([Account_UserName])
    REFERENCES [dbo].[Accounts]
        ([UserName])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AccountPost'
CREATE INDEX [IX_FK_AccountPost]
ON [dbo].[Posts]
    ([Account_UserName]);
GO

-- Creating foreign key on [Account_UserName] in table 'Comments'
ALTER TABLE [dbo].[Comments]
ADD CONSTRAINT [FK_AccountComment]
    FOREIGN KEY ([Account_UserName])
    REFERENCES [dbo].[Accounts]
        ([UserName])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AccountComment'
CREATE INDEX [IX_FK_AccountComment]
ON [dbo].[Comments]
    ([Account_UserName]);
GO

-- Creating foreign key on [Accounts_UserName] in table 'AccountOrganization'
ALTER TABLE [dbo].[AccountOrganization]
ADD CONSTRAINT [FK_AccountOrganization_Account]
    FOREIGN KEY ([Accounts_UserName])
    REFERENCES [dbo].[Accounts]
        ([UserName])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Organizations_ID] in table 'AccountOrganization'
ALTER TABLE [dbo].[AccountOrganization]
ADD CONSTRAINT [FK_AccountOrganization_Organization]
    FOREIGN KEY ([Organizations_ID])
    REFERENCES [dbo].[Organizations]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AccountOrganization_Organization'
CREATE INDEX [IX_FK_AccountOrganization_Organization]
ON [dbo].[AccountOrganization]
    ([Organizations_ID]);
GO

-- Creating foreign key on [Posts_ID] in table 'PostTag'
ALTER TABLE [dbo].[PostTag]
ADD CONSTRAINT [FK_PostTag_Post]
    FOREIGN KEY ([Posts_ID])
    REFERENCES [dbo].[Posts]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Tags_ID] in table 'PostTag'
ALTER TABLE [dbo].[PostTag]
ADD CONSTRAINT [FK_PostTag_Tag]
    FOREIGN KEY ([Tags_ID])
    REFERENCES [dbo].[Tags]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PostTag_Tag'
CREATE INDEX [IX_FK_PostTag_Tag]
ON [dbo].[PostTag]
    ([Tags_ID]);
GO

-- Creating foreign key on [Post_ID] in table 'Comments'
ALTER TABLE [dbo].[Comments]
ADD CONSTRAINT [FK_CommentPost]
    FOREIGN KEY ([Post_ID])
    REFERENCES [dbo].[Posts]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CommentPost'
CREATE INDEX [IX_FK_CommentPost]
ON [dbo].[Comments]
    ([Post_ID]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------