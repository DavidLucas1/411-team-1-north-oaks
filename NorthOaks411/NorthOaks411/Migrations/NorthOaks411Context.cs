﻿using System.Data.Entity;

namespace NorthOaks411.Migrations
{
    public class NorthOaks411Context : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, add the following
        // code to the Application_Start method in your Global.asax file.
        // Note: this will destroy and re-create your database with every model change.
        // 
        // System.Data.Entity.Database.SetInitializer(new System.Data.Entity.DropCreateDatabaseIfModelChanges<NorthOaks411.Migrations.NorthOaks411Context>());

        public NorthOaks411Context() : base("name=NorthOaks411Context")
        {
        }

        public DbSet<Account> Accounts { get; set; }

        public DbSet<Comment> Comments { get; set; }

        public DbSet<Organization> Organizations { get; set; }

        public DbSet<Post> Posts { get; set; }

        public DbSet<ApiSessionKey> ApiSessionKeys { get; set; }
    }
}
