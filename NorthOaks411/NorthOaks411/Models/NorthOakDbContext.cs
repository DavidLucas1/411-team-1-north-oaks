﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace NorthOaks411.Models
{
    public class NorthOakDbContext : DbContext 
    {
        public DbSet<Account> Account { get; set; }
        public DbSet<Comment> Comment { get; set; }
        public DbSet<Organization> Organization { get; set; }
        public DbSet<Post> Post { get; set; }
    }
}