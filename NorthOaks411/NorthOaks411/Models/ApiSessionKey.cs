﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NorthOaks411.Models
{
    public class ApiSessionKey
    {
        public int Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public String Key { get; set; }
    }
}