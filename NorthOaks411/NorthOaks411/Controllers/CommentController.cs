﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using NorthOaks411.Migrations;
using System.Web.Http.Cors;

namespace NorthOaks411.Controllers
{
     [EnableCors(origins: "http://localhost:49292", headers: "*", methods: "*")]
    public class CommentController : AbstractController
    {
        // GET api/Comment
        [HttpGet]
        public HttpResponseMessage GetComments()
        {
            return OK<List<Comment>>(db.Comments.ToList());
        }

        // GET api/Comment/5
        [HttpGet]
        public HttpResponseMessage GetComment(int id)
        {
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return NotFound();
            }

            return OK<Comment>(comment);
        }

        // PUT api/Comment/5
        [HttpPut]
        public HttpResponseMessage PutComment(int id, Comment comment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != comment.ID)
            {
                return BadRequest();
            }

            db.Entry(comment).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return NotFound(ex);
            }

            return OK();
        }

        // POST api/Comment
        [HttpPost]
        public HttpResponseMessage PostComment(Comment comment)
        {
            if (ModelState.IsValid)
            {
                db.Comments.Add(comment);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, comment);
                response.Headers.Location = new Uri(Url.Link("NorthOaksApi", new { id = comment.ID }));
                return response;
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        // DELETE api/Comment/5
        [HttpDelete]
        public HttpResponseMessage DeleteComment(int id)
        {
            Comment comment = db.Comments.Find(id);
            if (comment == null)
            {
                return NotFound();
            }

            db.Comments.Remove(comment);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return NotFound(ex);
            }

            return OK(comment);
        }
    }
}