﻿using NorthOaks411.Migrations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace NorthOaks411.Controllers
{
     [EnableCors(origins: "http://localhost:49292", headers: "*", methods: "*")]
    public abstract class AbstractController : ApiController
    {
        protected NorthOaksDB db = new NorthOaksDB();

        protected bool validateUser(String apiKey)
        {
            apiKey APIKEY = db.apiKeys.Where(k => k.key == apiKey).FirstOrDefault();

            if (APIKEY == null) return false;

            return true;
        }
        protected HttpResponseMessage OK(Object obj = null)
        {
            return Request.CreateResponse(HttpStatusCode.OK, obj);
        }

        protected HttpResponseMessage OK<T>(T obj)
        {
            return Request.CreateResponse(HttpStatusCode.OK, (T)obj);
        }

        protected HttpResponseMessage Created(Object obj = null)
        {
            return Request.CreateResponse(HttpStatusCode.Created, obj);
        }

        protected HttpResponseMessage NotFound(Object obj = null)
        {
            return Request.CreateResponse(HttpStatusCode.NotFound, obj);
        }

        protected HttpResponseException NotFoundException(Object obj = null)
        {
            return new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound, obj));
        }


        protected HttpResponseMessage BadRequest(Object obj = null)
        {
            return Request.CreateResponse(HttpStatusCode.BadRequest, obj);
        }

        protected HttpResponseException BadRequestException(Object obj = null)
        {
            return new HttpResponseException(Request.CreateResponse(HttpStatusCode.BadRequest, obj));
        }

        protected HttpResponseMessage Unauthorized(Object obj = null)
        {
            return Request.CreateResponse(HttpStatusCode.Unauthorized, obj);
        }

        protected HttpResponseException UnauthorizedException(Object obj = null)
        {
            return new HttpResponseException(Request.CreateResponse(HttpStatusCode.Unauthorized, obj));
        }
        


        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}