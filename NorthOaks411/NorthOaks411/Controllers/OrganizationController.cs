﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using NorthOaks411.Migrations;
using System.Web.Http.Cors;

namespace NorthOaks411.Controllers
{
     [EnableCors(origins: "http://localhost:49292", headers: "*", methods: "*")]
    public class OrganizationController : AbstractController
    {
        // GET api/Organization
        [HttpGet]
        public HttpResponseMessage GetOrganizations()
        {
            return OK<List<Organization>>(db.Organizations.ToList());
        }

        // GET api/Organization/5
        [HttpGet]
        public HttpResponseMessage GetOrganization(int id)
        {
            Organization organization = db.Organizations.Find(id);
            if (organization == null)
            {
                return NotFound();
            }

            return OK<Organization>(organization);
        }

        // PUT api/Organization/5
        [HttpPut]
        public HttpResponseMessage PutOrganization(int id, Organization organization)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != organization.ID)
            {
                return BadRequest();
            }

            db.Entry(organization).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return NotFound(ex);
            }

            return OK();
        }

        // POST api/Organization
        [HttpPost]
        public HttpResponseMessage PostOrganization(Organization organization)
        {
            if (ModelState.IsValid)
            {
                db.Organizations.Add(organization);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, organization);
                response.Headers.Location = new Uri(Url.Link("NorthOaksApi", new { id = organization.ID }));
                return response;
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        // DELETE api/Organization/5
        [HttpDelete]
        public HttpResponseMessage DeleteOrganization(int id)
        {
            Organization organization = db.Organizations.Find(id);
            if (organization == null)
            {
                return NotFound();
            }

            db.Organizations.Remove(organization);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return NotFound(ex);
            }

            return OK(organization);
        }
    }
}