﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using NorthOaks411.Migrations;
using NorthOaks411.DTO;
using System.Web.Http.Cors;

namespace NorthOaks411.Controllers
{
    [EnableCors(origins: "http://localhost:49292", headers: "*", methods: "*")]
    public class PostController : AbstractController
    {
        // GET api/{apiKey}/Post
        [HttpGet]
        public HttpResponseMessage GetPosts()
        {
            List<PostDTO> postDTOs = new List<PostDTO>();
            
            foreach (Post p in db.Posts)
            {
                PostDTO pDTO = new PostDTO(p);
                Account account = db.Accounts.Find(p.Account);
                if (account != null) pDTO.UserName = account.UserName;
                
                List<Comment> comments = db.Comments.Where(c => c.Post.ID == p.ID).ToList();
                List<CommentDTO> commentDtos = new List<CommentDTO>();
                foreach (Comment c in comments)
                {
                    CommentDTO cDto = new CommentDTO(c);
                    Account cAccount = db.Accounts.Find(c.Account);
                    cDto.UserName = cAccount.UserName;
                    commentDtos.Add(cDto);
                }
                pDTO.Comments = commentDtos;
                
                postDTOs.Add(pDTO);
            }

            return OK<List<PostDTO>>(postDTOs);                    
        }

        // GET api/Post/5
        [HttpGet]
        public HttpResponseMessage GetPost(int id)
        {
            Post post = db.Posts.Include(p => p.Comments)
                .Where(p => p.ID == id).FirstOrDefault();

            if (post == null)
            {
                return NotFound();
            }
            
            return OK<Post>(post);
        }

        // PUT api/Post/5
        [HttpPut]
        public HttpResponseMessage PutPost(int id, Post post)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != post.ID)
            {
                return BadRequest();
            }

            db.Entry(post).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return NotFound(ex);                
            }

            return OK();
        }

        // POST api/Post
        [HttpPost]
        public HttpResponseMessage PostPost(Post post)
        {
            if (ModelState.IsValid)
            {
                db.Posts.Add(post);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, post);
                response.Headers.Location = new Uri(Url.Link("NorthOaksApi", new { id = post.ID }));
                return response;
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        // DELETE api/Post/5
        [HttpDelete]
        public HttpResponseMessage DeletePost(int id)
        {  
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return NotFound();
            }

            db.Posts.Remove(post);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return NotFound(ex);
            }

            return OK(post);
        }
    }
}