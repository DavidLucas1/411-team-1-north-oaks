﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using NorthOaks411.Migrations;
using System.Web.Http.Cors;

namespace NorthOaks411.Controllers
{
     [EnableCors(origins: "http://localhost:49292", headers: "*", methods: "*")]
    public class AccountController : AbstractController
    {     

        // GET api/Account
        [HttpGet]
        public HttpResponseMessage GetAccounts()
        {
            return OK<List<Account>>(db.Accounts.Include(a => a.Comments).Include(a => a.Posts).ToList());
        }

        // GET api/Account/5
        [HttpGet]
        public HttpResponseMessage GetAccount(String id)
        {
            Account account = db.Accounts
                .Include(a => a.Posts)
                .Include(a => a.Comments)
                .Where(a => a.UserName == id).FirstOrDefault();

            if (account == null)
            {
                return NotFound();
            }

            return OK<Account>(account);
        }

        // PUT api/Account/5
        [HttpPut]
        public HttpResponseMessage PutAccount(String id, Account account)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != account.UserName)
            {
                return BadRequest();
            }

            db.Entry(account).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return NotFound(ex);
            }

            return OK();
        }

        // POST api/Account
        [HttpPost]
        public HttpResponseMessage PostAccount(Account account)
        {
            if (ModelState.IsValid)
            {
                db.Accounts.Add(account);
                db.SaveChanges();

                HttpResponseMessage response = Created(account);
                response.Headers.Location = new Uri(Url.Link("NorthOaksApi", new { id = account.UserName }));
                return response;
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        // DELETE api/Account/5
        [HttpDelete]
        public HttpResponseMessage DeleteAccount(String id)
        {
            Account account = db.Accounts.Find(id);
            if (account == null)
            {
                return NotFound();
            }

            db.Accounts.Remove(account);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return NotFound(ex);
            }

            return OK(account);
        }

       
    }
}