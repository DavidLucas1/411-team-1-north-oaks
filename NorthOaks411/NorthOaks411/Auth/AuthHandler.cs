﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Threading;
using System.Threading.Tasks;
using NorthOaks411.Migrations;
using System.Net;

namespace NorthOaks411.Auth
{
    public class AuthHandler : DelegatingHandler
    {
        protected NorthOaksDB db = new NorthOaksDB();

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (!ValidateRequest(request))
            {
                var response = new HttpResponseMessage(HttpStatusCode.Forbidden);
                var tsc = new TaskCompletionSource<HttpResponseMessage>();
                tsc.SetResult(response);
                return tsc.Task;
            }

            return base.SendAsync(request, cancellationToken);
        }

        

        private bool ValidateRequest(HttpRequestMessage request)
        {
            var headers = request.Headers;

            String apiKey = headers
                .FirstOrDefault(h => h.Key == "Api-Key")
                .Value.FirstOrDefault();

            if (apiKey == null || apiKey.Trim() == "") return false;

          //  apiKey sessionKey = db.apiKeys.FirstOrDefault(k => k.key == apiKey);
            apiKey APIKEY = new apiKey();
            APIKEY.key = "tlscwUlGSOlgPosOfbcZiww";
            if (APIKEY.key != apiKey) return false;

            return APIKEY != null;
        }
    }
}