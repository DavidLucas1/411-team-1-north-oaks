﻿using NorthOaks411.Auth;
using NorthOaks411.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Http.Cors;
namespace NorthOaks411
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            DelegatingHandler[] AuthHandler = new DelegatingHandler[] { new AuthHandler() };
            HttpMessageHandler authRouteHandler = HttpClientFactory.CreatePipeline(new HttpControllerDispatcher(config), AuthHandler);

            // Enable Cross-Origin Requests
            // doc: http://www.asp.net/web-api/overview/security/enabling-cross-origin-requests-in-web-api
          //  var cors = new EnableCorsAttribute("*", "*", "*");
          //  conifg.EnableCors();
            var cors = new EnableCorsAttribute("*", "*", "8");
            config.EnableCors(cors);

            config.Routes.MapHttpRoute(
                name: "NorthOaksApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional },
                constraints: null,
                handler: authRouteHandler
            );
           
            // Uncomment the following line of code to enable query support for actions with an IQueryable or IQueryable<T> return type.
            // To avoid processing unexpected or malicious queries, use the validation settings on QueryableAttribute to validate incoming queries.
            // For more information, visit http://go.microsoft.com/fwlink/?LinkId=279712.
            //config.EnableQuerySupport();

            // To disable tracing in your application, please comment out or remove the following line of code
            // For more information, refer to: http://www.asp.net/web-api
            config.EnableSystemDiagnosticsTracing();
        }
    }
}
