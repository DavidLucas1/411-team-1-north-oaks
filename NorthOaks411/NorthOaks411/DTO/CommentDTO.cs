﻿using NorthOaks411.Migrations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NorthOaks411.DTO
{
    public class CommentDTO
    {
        public int Id { get; set; }
        public String UserName { get; set; }
        public string Text { get; set; }
        public DateTime CreateDate { get; set; }

        public CommentDTO() { }

        public CommentDTO(Comment comment)
        {
            this.Id = comment.ID;
            this.Text = comment.Text;
            this.CreateDate = comment.CreateDate;
        }
    }
}