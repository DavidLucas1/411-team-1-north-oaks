﻿using NorthOaks411.Migrations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NorthOaks411.DTO
{
    public class PostDTO
    {
        public int ID { get; set; }
   //     public String Title { get; set; }
        public List<Tag> Tags { get; set; }        
        public String UserName { get; set; }
        public String Text { get; set; }
        public DateTime ReleaseDate { get; set; }

        public List<CommentDTO> Comments { get; set; }

        public PostDTO() { }

        public PostDTO(Post post)
        {
            this.ID = post.ID;
          //  this.Title = post.Title;
            this.Tags = post.Tags.ToList();
            this.Text = post.Text;
            this.ReleaseDate = post.ReleaseDate;
            this.UserName = "N/A";
        }
    }
}