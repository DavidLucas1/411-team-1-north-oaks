﻿using NurseryS.App_Start;
using NurseryS.Context;
using NurseryS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace NurseryS
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class AuthorizeAttribute : System.Web.Mvc.AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(System.Web.Mvc.AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Request.IsAuthenticated)
            {
                filterContext.Result = new System.Web.Mvc.HttpStatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
            }
            else
            {
                base.HandleUnauthorizedRequest(filterContext);
            }
        }
    }
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
         
        
        }
        /*
        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();

            // ... log error here

            var httpEx = exception as HttpException;

            if (httpEx != null && httpEx.GetHttpCode() == 403)
            {
                Response.Redirect("/Error/Forbidden", true);
            }
            else if (httpEx != null && httpEx.GetHttpCode() == 404)
            {
                Response.Redirect("/Error/NotFound", true);
            }
            else
            {
                Response.Redirect("/Error/Application", true);
            }
        }
         * */
        protected void Application_PostAuthenticateRequest(Object sender, EventArgs args)
        {
            if (FormsAuthentication.CookiesSupported == true)
            {
                if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
                {
                    try
                    {
                        //let us take out the username now                
                        string username = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;
                        Role role = null;

                        using (DataContext dbContext = new DataContext())
                        {
                            Account user = dbContext.Accounts.SingleOrDefault(u => u.UserName == username);

                            role = user.Role;
                        }
                        //let us extract the roles from our own custom cookie
                        
                        //Let us set the Pricipal with our user specific details
                        HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(
                          new System.Security.Principal.GenericIdentity(username, "Forms"), new string[]{role.RoleName});
                    }
                    catch (Exception)
                    {
                        //something went wrong
                    }
                }
            }
        }
    }

}