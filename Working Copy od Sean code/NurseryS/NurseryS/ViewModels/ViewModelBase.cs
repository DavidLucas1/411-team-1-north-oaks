﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NurseryS.ViewModels
{
    public abstract class ViewModelBase
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool logout { get; set; }
    }
}