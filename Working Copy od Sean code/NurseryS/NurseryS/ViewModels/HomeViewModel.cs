﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NurseryS.Models;

namespace NurseryS.ViewModels
{
    public class HomeViewModel : ViewModelBase
    {
        public List<Post> posts { get; set; }

    }
}