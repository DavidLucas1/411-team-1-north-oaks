﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NurseryS.ViewModels
{
    public class ErrorViewModel : ViewModelBase
    {
        public string Message { get; set; }
        public string Description { get; set; }
    }
}