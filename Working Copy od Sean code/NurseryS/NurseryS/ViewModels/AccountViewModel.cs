﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NurseryS.Models;
using System.ComponentModel.DataAnnotations;


namespace NurseryS.ViewModels
{
    public class AccountViewModel : ViewModelBase
    {
        public Account account { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [StringLength(255, ErrorMessage = "Must be between 5 and 255 characters", MinimumLength = 5)]
        [DataType(DataType.Password)]
        [Display(Name="Password")]
        public string passTemp { get; set; }

        [Required(ErrorMessage = "Reenter Password is required")]
        [StringLength(255, ErrorMessage = "Must be between 5 and 255 characters", MinimumLength = 5)]
        [DataType(DataType.Password)]
        [Compare("passTemp")]
        [Display(Name="ConfirmPassword")]
        public string passReTemp { get; set; }
    }
}