﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NurseryS.Models;
using NurseryS.Context;
using NurseryS.ViewModels;
using System.Web.Security;

namespace NurseryS.Controllers
{
    public class AccountController : Controller
    {
        private DataContext db = new DataContext();

        //
        // GET: /Account/

        public ActionResult Index()
        {
            return View(db.Accounts.ToList());
        }

        //
        // GET: /Account/Details/5


        public ActionResult Details(string username = null)
        {
            if(username == null && User.Identity.IsAuthenticated)
            {
                username = User.Identity.Name;
            }
            else if(username == null && !User.Identity.IsAuthenticated)
            {
                return HttpNotFound();
            }
            Account account = db.Accounts.FirstOrDefault(ac => ac.UserName.Equals(username));
 
            if (account == null)
            {
                return HttpNotFound();
            }

            AccountViewModel avm = new AccountViewModel();
            avm.account = account;
            return View(avm);
        }

        //
        // GET: /Account/Create

        //[RequireHttps]
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Account/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        //[RequireHttps]
        public ActionResult Create(AccountViewModel avm, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                
                avm.account.Register(avm.account.UserName, avm.passTemp, avm.account.FirstName, avm.account.LastName, avm.account.Email);

                
                if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                    && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                {
                    return Redirect(returnUrl);
                }
                else
                {
                 
                    return RedirectToAction("Index", "Home");
                }
            }

            return View(avm);
        }

        public ActionResult Login()
        {
            //LoginViewModel lm = new LoginViewModel();
            return View();
        }

        //
        // POST: /Account/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel lm, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                bool success = Account.Login(lm.UserName, lm.Password);

                if (success)
                {
                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                        && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }

            }

            return View(lm);
        }

        public ActionResult Logoff()
        {
            Account.Logout();

            //LoginViewModel lm = new LoginViewModel();
            Session.Clear();
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/Edit/5

        public ActionResult Edit(string id = null)
        {
            Account account = db.Accounts.Find(id);
            if (account == null)
            {
                return HttpNotFound();
            }
            return View(account);
        }

        //
        // POST: /Account/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Account account)
        {
            if (ModelState.IsValid)
            {
                db.Entry(account).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(account);
        }

        //
        // GET: /Account/Delete/5

        public ActionResult Delete(string id = null)
        {
            Account account = db.Accounts.Find(id);
            if (account == null)
            {
                return HttpNotFound();
            }
            return View(account);
        }

        //
        // POST: /Account/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Account account = db.Accounts.Find(id);
            db.Accounts.Remove(account);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}