﻿using NurseryS.Context;
using NurseryS.Models;
using NurseryS.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NurseryS.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        DataContext db = new DataContext();

        public ActionResult Index()
        {
            HomeViewModel hmv = new HomeViewModel();
            hmv.posts = db.Posts.Where(post => DateTime.Compare(post.ReleaseDate, DateTime.Now) < 0).ToList();
            return View(hmv);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(HomeViewModel hmv)
        {

            if (hmv.UserName != null && hmv.Password != null)
            {
                Account.Login(hmv.UserName, hmv.Password);
            }

        
            return View();
        }


    }
}
