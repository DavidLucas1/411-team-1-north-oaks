namespace NurseryS.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Role
    {
        public Role()
        {
            this.Accounts = new HashSet<Account>();
        }
    
        public int ID { get; set; }
        public string RoleName { get; set; }
    
        public virtual ICollection<Account> Accounts { get; set; }
    }
}
