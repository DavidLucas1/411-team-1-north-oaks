namespace NurseryS.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Comment
    {
        public int ID { get; set; }
        public string Text { get; set; }
        public System.DateTime CreateDate { get; set; }
    
        public virtual Account Account { get; set; }
        public virtual Post Post { get; set; }
    }
}
