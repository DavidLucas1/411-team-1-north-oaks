namespace NurseryS.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Post
    {
        public Post()
        {
            this.Tags = new HashSet<Tag>();
        }
    
        public int ID { get; set; }
        public System.DateTime CreateDate { get; set; }
        public System.DateTime ReleaseDate { get; set; }
        public string Text { get; set; }
    
        public virtual Account Account { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }
}
