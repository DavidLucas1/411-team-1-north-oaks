namespace NurseryS.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Organization
    {
        public Organization()
        {
            this.Accounts = new HashSet<Account>();
        }
    
        public int ID { get; set; }
        public string Name { get; set; }
        public string Website { get; set; }
        public string Description { get; set; }
    
        public virtual ICollection<Account> Accounts { get; set; }
    }
}
