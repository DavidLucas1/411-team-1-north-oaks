namespace NurseryS.Models
{
    using System;
    using System.Collections.Generic;
    using NurseryS.Context;
    using System.Security.Cryptography;
    using System.Text;
    using System.Web.Security;
    using System.Web;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    
    
    public partial class Account
    {
        
    
        public string UserName { get; set; }
        public byte[] Password { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public byte[] Salt { get; set; }
    
        public virtual Role Role { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Organization> Organizations { get; set; }


        public Account()
        {
            this.Posts = new HashSet<Post>();
            this.Comments = new HashSet<Comment>();
            this.Organizations = new HashSet<Organization>();
        }

        public static bool Login(string userName, string password)
        {
            DataContext db = new DataContext();
            Account user = db.Accounts.Find(userName);
            if(user == null)
            {
                return false;
            }

            byte[] salt = user.Salt;
            byte[] preHash = new byte[salt.Length + password.Length];
            int i = 0;
            for (i = 0; i < salt.Length; i++)
            {
                preHash[i] = salt[i];
            }
            for (int j = 0; j < password.Length; j++, i++)
            {
                preHash[i] = Convert.ToByte(password[j]);
            }
            SHA256 sha256 = SHA256Managed.Create();
            byte[] hashValue = sha256.ComputeHash(preHash);

            bool equality = true;
            for (i = 0; i < user.Password.Length; i++)
            {
                if(!user.Password[i].Equals(hashValue[i]))
                {
                    equality = false;
                    break;
                }
            }
            if(equality) 
            {
                // user is correct
                FormsAuthentication.SetAuthCookie(user.UserName, false);

                
                return true;
            }
            return true;
        }

        public static void Logout()
        {
            FormsAuthentication.SignOut();
        }

        public void Register(string userName, string password, string firstName, string lastName, string email)
        {
            DataContext db = new DataContext();
            Account newUser = new Account();
            newUser.UserName = userName;
            newUser.FirstName = firstName;
            newUser.LastName = lastName;
            newUser.Email = email;

            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] salt = new byte[32];
            rng.GetBytes(salt);
            byte[] preHash = new byte[32 + password.Length];
            //string saltString = Encoding.UTF8.GetChars(salt).ToString();
            //password = saltString + password;
            int i = 0;
            for(i = 0; i < salt.Length; i++)
            {
                preHash[i] = salt[i];
            }
            for (int j = 0; j < password.Length; j++, i++)
            {
                preHash[i] = Convert.ToByte(password[j]);
            }
            
            SHA256 sha256 = SHA256Managed.Create();
            byte[] hashValue = sha256.ComputeHash(preHash);

            newUser.Password = hashValue;
            newUser.Salt = salt;

            newUser.Role = db.Roles.Find(2);

            db.Accounts.Add(newUser);
            db.SaveChanges();

            FormsAuthentication.SetAuthCookie(newUser.UserName, false);
        }
        
    }
}
